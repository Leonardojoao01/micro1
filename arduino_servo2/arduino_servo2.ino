#include "Ultrasonic.h"
#include <VirtualWire.h>
#include <OneWire.h>
#include <DallasTemperature.h>


const int button_Pin = 2;     // Pino do pushbutton
const int led_Pin =  13;      // Pino para ligar LED
const int ldr_Pin = A0;      //Pino analógico em que o sensor está conectado.
const int ldr_alarme_pin = A1;  
const int cooler_Pin = 9; //Pino para ligar cooler
const int aquec_Pin = 8; //Pino pro aquecedor
const int buzzer_Pin = 10; //Pino pro buzzer

int valorSensor = 0; //variável usada para ler o valor do sensor em tempo real.
//char dt;
float dist;
//------------------------------------------------------
char End_Arduino[] = "0";        // ID do arduino
char End_Atuador[] = "0";        // Para saber qual atuador está sendo usado
char LDR[] = "0";// Para saber se atuador está ativo
char BUTTON[] = "0";
char ULTRA[] = "0";
char End_Sensor[] = "0";         // Para saber qual sensor está sendo usado
char Status_Sensor[] = "0";      // Para saber se sensor está ativo
//------------------------------------------------------

//--------------Sensor de temp--------
#define ONE_WIRE_BUS 3

OneWire oneWire(ONE_WIRE_BUS);

DallasTemperature sensors(&oneWire);

DeviceAddress waterThermometer = { 0x28, 0xF8, 0x7C, 0xA2, 0x03, 0x00, 0x00, 0xCB };
bool flag=false;
//------------------------------


//------------Valores_p/_string_mensagem----------------
char val_0[] = "0";
char val_1[] = "1";
//------------------------------------------------------


//--------------Endereço_do_Arduinos------------------
char Arduino = '2';
int arduino = 2;
//------------------------------------------------------

//--------------Endereços_dos_Atuadores-----------------
char button[] = "1";
//------------------------------------------------------

//--------------Endereços_dos_Sensores------------------
char luminosidade[] = "1"; 
char ultrassom[] = "2";
char optico[] = "3";
//------------------------------------------------------

//-------------Ranges do LDR---------------------------
char ldr_range[] = "ABCDE0";  // A = > 1000 (muito escuro) ;  1000 > B > 800 (escuro) ; 800 > C > 500 (luz ambiente) ; 500 > D > 200 (luminosidade alta); E < 200 (luminosidade altissima) ; 0 - excecao problema no circuito ou circuito inexistente

//------------------------------------------------------
char mensagem[10];   // Usado para enviar dados 
char info[30];      // Usado para receber dados
//------------------------------------------------------
boolean estado = false;
boolean alarme_state = false;
boolean aquec_state = false;
//------------------------------------------------------
Ultrasonic ultrasonic(3,4);// Pinos do ultrassom (echo,  trig) ????? Verificar a ordem!
//------------------------------------------------------

void setup(){
  pinMode(led_Pin, OUTPUT);   // Saida para ligar o LED  
  pinMode(cooler_Pin,  OUTPUT);
  pinMode(aquec_Pin,  OUTPUT);
  pinMode(buzzer_Pin,  OUTPUT);
  pinMode(button_Pin, INPUT); // Entrada do sinal botton
  sensors.begin();
  sensors.setResolution(waterThermometer, 10);
  Serial.begin(9600);
  vw_set_ptt_inverted(true); // Defaut da biblioteca do RF
  vw_setup(2000);            // Defaut da biblioteca do RF
  vw_rx_start();    //Inicializa o receptor 
}
//------------------------------------------------------

void loop(){
           // Verifica se o button foi pressionado
  
  
  uint8_t buf[VW_MAX_MESSAGE_LEN];  
  uint8_t buflen = VW_MAX_MESSAGE_LEN;
  if (!vw_get_message(buf, &buflen)){           //Verifica se recebe informação  
     char button = codificaButton();
     char distancia = codificaDist();
     char ldr = codificaRangeLum();
     char cooler = codificaCooler();
     char aquec = codificaAquec();
     char buzzer = codificaBuzzer();
     char alarme = codificaAlarme();
     char temp_ar = 'X';
     envio(Arduino, cooler, aquec, buzzer, alarme, ldr, temp_ar, distancia, button);
  }
  else{
    if(buflen < 6){
      for (int i = 0; i < buflen; i++){
           info[i]=buf[i];
      }

      if(info[0] == Arduino ){
          switch (info[1]){
            case 'L' : digitalWrite(cooler_Pin, 1); break;
            case 'D': digitalWrite(cooler_Pin, 0); break;
            default: break;
          }
          switch (info[2]){
            case 'L' : aquec_state = true; break;
            case 'D': aquec_state = false; break;
            default: break;
          }
          switch (info[3]){
            case 'L' : digitalWrite(buzzer_Pin, 1); break;
            case 'D': digitalWrite(buzzer_Pin, 0); break;
            default: break;
          }
          switch (info[4]){
            case 'L' : alarme_state = true; break;
            case 'D': alarme_state = false; break;
            default: break;
          }
      }
   }
  }
  
  verificaAlarme();
  verificaAquec(waterThermometer);

}
  //--------------------------------------------------------------------------------------------------------------------  
  void verificaAlarme(){
    int alarm_value = analogRead(ldr_alarme_pin);
    if(alarm_value < 1000 && alarme_state){
      digitalWrite(buzzer_Pin, 1);
    }

  }
 
  void verificaAquec(DeviceAddress deviceAddress)
{
  sensors.requestTemperatures();
  float tempC = sensors.getTempC(deviceAddress);
  
  if((tempC < 80 && !flag )&&(aquec_state){
    digitalWrite(aquec_Pin, 1);
    flag = true;
  } else{
      digitalWrite(aquec_Pin, 0);
      if (tempC < 80 || tempC > 70){
       
        } else{
          flag = false;
        }

  }
}
 //-------------------------------------------------------------------------------------------------------------------- 
char codificaDist(){
  
  char dt;
  float distanc = ultrasonic.Ranging(CM);
  Serial.println(distanc);
  
  if( distanc > 5 && distanc <= 10){
    dt = 'A';
    }
  else if( distanc > 10 && distanc <= 20){
    dt = 'B';
    }
  else if( distanc > 20 && distanc <= 40){
    dt = 'C';
    }
  else{
    dt = '0';
  }
  delay(1000);
  return dt; 
  
}

char codificaAlarme(){
  if(alarme_state){
    return 'L';
  }
  else{
    return 'D';
  }

}

char codificaCooler(){
  int state = digitalRead(cooler_Pin);
  if(state == 0){
    return 'D';
  }
  else{
    return 'L';
  }
}

char codificaAquec(){
  int state = digitalRead(aquec_Pin);
  if(state == 0){
    return 'D';
  }
  else{
    return 'L';
  }
}

char codificaBuzzer(){
  int state = digitalRead(buzzer_Pin);
  if(state == 0){
    return 'D';
  }
  else{
    return 'L';
  }
}

char codificaRangeLum(void){
  int valorSensor = analogRead(0);
  char lum;
  
  if(valorSensor > 500 && valorSensor < 800){
    lum = 'C'; 
  }
  else if(valorSensor > 800 && valorSensor < 1000){
    lum = 'B';
  }
  else if(valorSensor > 200 && valorSensor < 500){
    lum = 'D';
  }
  else if(valorSensor > 1000){
    lum = 'A';
  }
  else if(valorSensor > 1 && valorSensor < 200){
    lum = 'E';
  }
  else{
    lum = '0';
  }
  return lum;
}

char codificaButton(){
  int buttonState = digitalRead(button_Pin);
  if(buttonState == 0){
    return 'D';
  }
  else{ 
    return 'L';
  }
}

void envio(char end_ard, char cooler, char aquec, char buzzer, char alarme, char ldr, char temp_ar, char distance, char button){
      
    if((LDR[0] == ldr && BUTTON[0] == button && ULTRA[0] == distance)){
         //Serial.println("Funcionando");
     }
     else{
         mensagem[0] = end_ard;
         mensagem[1] = cooler;
         mensagem[2] = aquec;
         mensagem[3] = buzzer;
         mensagem[4] = alarme;
         mensagem[5] = ldr;
         mensagem[6] = temp_ar;
         mensagem[7] = distance;
         mensagem[8] = button;
     
         ULTRA[0] = distance;
         LDR[0] = ldr; 
         BUTTON[0] = button;
         
         Serial.println(mensagem);
         
         char *msg = mensagem;
         vw_send((uint8_t *)msg, strlen(msg));
         vw_wait_tx();
   }
}
