#define LED_Amarelo 5 //Define LED_Amarelo como 5
#define LED_Vermelho 6 //Define LED_Vermelho como 6
#define Lamp 7 //Define Lamp como 7

//#include <Thermistor.h> // Inclui a biblioteca Thermistor.h

#include <SPI.h>          // Inclui a biblioteca SPI.h
#include <Ethernet.h>     // Inclui a biblioteca Ethernet.h
//#include <VirtualWire.h>  // Inclui a biblioteca VirtualWire.h

//------------------------------------------------------
char End_Arduino[] = "0";        // ID do arduino
char End_Atuador[] = "0";        // Para saber qual atuador está sendo usado
char Status_Atuador[] = "0";     // Para saber se atuador está ativo


char mensagem[4];                // Salva bytes para envio
char msg;                        // O que é enviado
char info[30];                   // Recebe a mensagem
//------------------------------------------------------

char x[]="a";
char y[]="b";
char z[]="c";
char w[]="d";
char t[]="e";

//======================================================================================
// Configurações para o Ethernet Shield
byte mac[] = { 0x90, 0xA2, 0xDA, 0x0D, 0x83, 0xEA }; // Entre com o valor do MAC

IPAddress ip(192,168,2,88);   
//IPAddress ipg(192,168,2,89);                      // Configure um IP válido 
byte gateway[] = { 192 , 168, 1, 1 };               //Entre com o IP do Computador onde a Câmera esta instalada
byte subnet[] = { 255, 255, 0, 0 };               //Entre com a Máskara de Subrede
EthernetServer server(80);                         //Inicializa a biblioteca EthernetServer com os valores de IP acima citados e configura a porta de acesso(80)
//======================================================================================

byte COD = B1000;

String A1_carga = "Motor Portao";  //Função do primeiro botão
String A2_carga = "Cooler"; //Função do segundo botão
String A3_carga = "Lampada";      //Função do terceiro botão

String B1_carga = "Lampada";
String B2_carga = "Ultrason";
String B3_carga = "Aquecedor Agua";

String C1_carga = "Luminosidade";
int C1_valor = 22 ;
String C2_carga = "Temperatura";
String C3_carga = "Temperatura";

boolean A1_estado=false; // Variável para armazenar o estado do primeiro botão
boolean A2_estado=false; // Variável para armazenar o estado do segundo botão
boolean A3_estado=false; // Variável para armazenar o estado do terceiro botão
       
boolean B1_estado=false; // Variável para armazenar o estado do primeiro botão
boolean B2_estado=false; // Variável para armazenar o estado do segundo botão
boolean B3_estado=false; // Variável para armazenar o estado do terceiro botão


void setup(){
    Serial.begin(9600);
    pinMode(LED_Amarelo,OUTPUT);   // Define o pino 5 como saída
    pinMode(LED_Vermelho,OUTPUT);  // Define o pino 6 como saída
    pinMode(Lamp,OUTPUT);          // Define o pino 7 como saída
    // Inicializa o Server com o IP e Mac atribuido acima
    Ethernet.begin(mac, ip);
  //  vw_set_rx_pin(5);
  //  vw_set_tx_pin(8);
  //  vw_set_ptt_inverted(true);     //   
  //  vw_setup(2000);                //Taxa de transferência  
  //  vw_rx_start();                 //Inicializa o receptor
}

void loop(){
      
  //acionamentos();                             //Vai para a função que executa o acionamento dos botões
  //regras();
  //int temperature = temp.getTemp();         //Pega a temperatura ambiente já convertida em graus celcius e armazena na variável temperature

  EthernetClient client = server.available(); // Verifica se tem alguém conectado

  if (client){
    boolean currentLineIsBlank = true;         // A requisição HTTP termina com uma linha em branco Indica o fim da linha
    String valPag;
    while (client.connected()){
      if (client.available()){

//=============================================================================================================================
        char c = client.read(); //Variável para armazenar os caracteres que forem recebidos
        valPag.concat(c); // Pega os valor após o IP do navegador ex: 192.168.1.2/0001        

        //Compara o que foi recebido
        if(valPag.endsWith("001000000")){ //Se o que for pego após o IP for igual a 0001
 
            A1_estado = !A1_estado;
             //Inverte o estado do primeiro acionamento
        }
        
        else if(valPag.endsWith("000001000")){ //Senão se o que for pego após o IP for igual a 0010
 
            A2_estado = !A2_estado; //Inverte o estado do segundo acionamento
        }
        
        else if(valPag.endsWith("000000001")){ //Senão se o que for pego após o IP for igual a 0100
          
            A3_estado = !A3_estado;  //Inverte o estado do terceiro acionamento      
        }
        else if(valPag.endsWith("010000000")){ //Senão se o que for pego após o IP for igual a 0100
           
            B1_estado = !B1_estado;  //Inverte o estado do terceiro acionamento      
        }
        



        else if(valPag.endsWith("11111")){
            client.println("HTTP/1.1 200 OK");
            client.println("Content-Type: text/html");
            client.println();
            client.print("<HTML> ");
            
            
            //client.print("<center>COD ATUAL: <B><font size=7> ");
            
//=========================================================            
            if(A1_estado == true){
              client.println("001000000:1");
            }
            else if (A1_estado == false){
              client.println("001000000:0");
            }
            client.print("<BR><BR>");
            if(A2_estado == true){
               client.println("000001000:1");
            }
            else if (A2_estado == false){
              client.println("000001000:0");
            }
//=========================================================
            client.println("</HTML>");
            delay(3);       // Espera um tempo para o navegador receber os dados
            client.stop();
           
        }
//=============================================================================================================================

        if (c == '\n' && currentLineIsBlank){
            //Inicia página HTML
            client.println("HTTP/1.1 200 OK");
            client.println("Content-Type: text/html");
            client.println();
            client.print("<HTML> ");
           //=========================================================================================================================
          
          client.print("<BR><BR>");
         client.println("<h2><center><table border=\"0\">");
         client.println("<tr><h1>");
         client.println("<th>Garagem</th><th>Quarto</th><th>Cozinha</th></font></tr>");
         client.println("<tr>");
         client.println("<td>");

        client.print("<center><button id='011' onclick=\"window.location.href='http://192.168.2.88/001000000'\">\0</button> > Codigo: 001000000 > ");
          if(A1_estado){            
            
            client.print("<B><span style=\"color: #00ff00;\">");   
            client.print(A1_carga);
            client.print(" - ON");     
            client.print("</span></B></center>");
          }         
          else{
          
            client.print("<B><span style=\"color: #ff0000;\">");
            client.print(A1_carga);
            client.print(" - OFF");
            client.print("</span></B></center>");
          }
          client.println("</td><td>");

          client.print("<center><button id='010' onclick=\"window.location.href='http://192.168.2.88/000001000'\">\0</button> > Codigo: 000001000 > ");
          if(A2_estado){
            client.print("<B><span style=\"color: #00ff00;\">");
            client.print(A2_carga);
            client.print(" - ON");
            client.print("</span></B></center>");
          }
          else{
            client.print("<B><span style=\"color: #ff0000;\">");
            client.print(A2_carga);
            client.print(" - OFF");
            client.print("</span></B></center>");
          }
           client.println("</td><td>");
          client.print("<center><button id='100' onclick=\"window.location.href='http://192.168.2.88/000000001'\">\0</button> > Codigo: 000000001 > ");
          if(A3_estado){
            client.print("<B><span style=\"color: #00ff00;\">");
            client.print(A3_carga);
            client.print(" - ON");
            client.print("</span></B></center> ");
          }
          else{
            client.print("<B><span style=\"color: #ff0000;\">");
            client.print(A3_carga);
            client.print(" - OFF");
            client.print("</span></B></center> ");
          } 
           client.println("</td>");
         
         client.println("</tr>");

         client.print("<tr>");
         client.print("<td>");
         
         Serial.println(C1_carga);
         client.print(C1_carga);
         client.print("-");
         client.print(C1_valor);
         client.print("</td><td>joao putinho</td><td>lala</td></tr>");
        
         client.println("</font></table></center></h2>");
          //Primeiro BOTAO
        
          //========================================================================================================================= 
          client.print("<BR><BR>");

          //COD ATUAL
          
          //=========================================================================================================================

          //client.print(" <meta http-equiv=\"refresh\" content=\"5; url=http://192.168.2.88/\"> ");

          client.println("</HTML>");

          break;
        }           //Fecha if (c == '\n' && currentLineIsBlank)
      }             //Fecha if (client.available())
    }               //Fecha While (client.connected())
    delay(3);       // Espera um tempo para o navegador receber os dados
    client.stop();  // Fecha a conexão
  }                 //Fecha if(client)
}
     
void garagem(EthernetClient client, String valPag){
          while(true){
            client.println("HTTP/1.1 200 OK");
            client.println("Content-Type: text/html");
            client.println();
            client.print("<HTML> ");
            
            Serial.println("Akiiiii");
            //client.print("<center>COD ATUAL: <B><font size=7> ");
            
//=========================================================            
            if(A1_estado == true){
              client.println("0001:1");
            }
            else if (A1_estado == false){
              client.println("0001:0");
            }
            client.print("<BR><BR>");
            if(A2_estado == true){
               client.println("0010:1");
            }
            else if (A2_estado == false){
              client.println("0010:0");
            }
//========================================================
            client.println("</HTML>");
            delay(3);       // Espera um tempo para o navegador receber os dados
            client.stop();
          }
}            //Fecha loop
/*
void acionamentos(){ //Abre função acionamento()
    uint8_t buf[VW_MAX_MESSAGE_LEN];  
    uint8_t buflen = VW_MAX_MESSAGE_LEN;
    if (vw_get_message(buf, &buflen)){   //Verifica se recebe informação  
       for (int i = 0; i < buflen; i++){
           info[i]=buf[i];

       if(A1_estado || buf[1]=='1' || buf[3]=='2' || buf[4]=='1'){  
           if(A1_estado == false){
               A1_estado = !A1_estado;
           }
           Serial.println("LIGA");
           digitalWrite(LED_Amarelo,HIGH);
           x[0] = '0';
           y[0] = '1';
           w[0] = '0';
           concatena_sinal(x[0],y[0],w[0]);
        }
        else{
         
           digitalWrite(LED_Amarelo,LOW);
           x[0] = '0';
           y[0] = '0';
           w[0] = '0';
           Serial.println("DESLIGA");
           concatena_sinal(x[0],y[0],w[0]);
        }
        if(A2_estado) digitalWrite(LED_Vermelho,HIGH);
        else digitalWrite(LED_Vermelho,LOW);
        if(A3_estado) digitalWrite(Lamp,HIGH);
        else digitalWrite(Lamp,LOW);
        }
     }
  //=========================================================================================================================
  
}*/
/*
char concatena_sinal(char x, char y, char z){
     mensagem[0] = x;
     mensagem[1] = y;
     mensagem[2] = z;
     
     End_Arduino[0] = x;
     End_Atuador[0] = y;
     Status_Atuador[0] = z;
     
     char *msg = mensagem;
    // vw_send((uint8_t *)msg, strlen(msg));
    // vw_wait_tx();
     //Serial.println(mensagem);
}
*/


