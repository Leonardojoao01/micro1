

// mensagem de 34 bytes (maximo) :  arduino_id, ativa_lcd, mensagem (se divide em duas de 16)
// include the library code:
#include <LiquidCrystal.h>
#include <VirtualWire.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
char arduino_id;
char ativa_lcd;
char final[17];
char final2[17];
//char ja;
int contador_serial = 0;
int cont = 0;
int cont2 = 0;


void setup(){
    // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  vw_set_rx_pin(8);
  // initialize the serial communications:
  Serial.begin(9600);
  
  vw_set_ptt_inverted(true);     //   
  vw_setup(2000);                //Taxa de transferência  
  vw_rx_start();                 //Inicializa o receptor
  
}

void loop()
{
  // when characters arrive over the serial port...
  uint8_t buf[VW_MAX_MESSAGE_LEN];  
  uint8_t buflen = VW_MAX_MESSAGE_LEN;
  if (vw_get_message(buf, &buflen)) {
    
    for (int i = 0; i < buflen; i++){
      if(i == 0){  //verifica se a mensagem  pra ele
        arduino_id = buf[i];
      }
      else{
        if(arduino_id == '1' && i == 1){   //verifica o que eh pra fazer no lcd
          ativa_lcd = buf[i];
          if(ativa_lcd == '0')
            lcd.clear();
        }
        else{
          if(arduino_id == '1' && ativa_lcd == '1'){
            char ja = buf[i];
            concatena(ja);  
          }
          else{
           char lixo = buf[i];  
          }
        }  
      }
    }
    if(arduino_id == '1' && ativa_lcd == '1'){
    lcd.setCursor(0, 0);
    lcd.print(final);
    if(cont > 16){
        lcd.setCursor(0, 1);
        lcd.print(final2);
    }
    //Serial.println(cont);
    cont = 0;
    cont2 = 0;
    limpaArrays();
    }
  }
}

void concatena(char ja){
  if(cont < 16){
      final[cont] = ja;
  }
  else{
      final2[cont2] = ja;
      cont2++;    
  }
  
  cont++; 
}

void limpaArrays(){
 
  for(int i = 0; i < 17; i++){
    final[i]=' ';
    final2[i]=' ';
  } 
  
}
