// arduino_mestre_new.ino
#include <VirtualWire.h>
#include <Stream.h>
String c;  //usado para receber dados seriais do raspberry
char mensagem[7];   // Usado para enviar dados por rf para os servos
char info[10];       //Usado para receber dados rf dos servos
String lcd_msg; //usado para enviar dados pro arduino 1 que contem o lcd


void setup(){
    pinMode(2,OUTPUT);
    
    Serial.begin(9600);
    vw_set_ptt_inverted(true); // Defaut da biblioteca do RF
    vw_setup(2000);            // Defaut da biblioteca do RF
    vw_rx_start();    //Inicializa o receptor 
}

void loop(){
  uint8_t buf[VW_MAX_MESSAGE_LEN];  
  uint8_t buflen = VW_MAX_MESSAGE_LEN;
  
  if(Serial.available()){
    c = Serial.readString();;
    digitalWrite(2, 1);
    
    //---------------------COZINHA--------------------------------------
    if(c == "cozlampl"){
    	mensagem[0] = '2';        //id do arduino da cozinha
    	mensagem[1] =  mensagem [2] = mensagem[3] = mensagem[4] = 'M';
    	mensagem[5] = 'L';
        digitalWrite(2, 1);
    	envio(mensagem);
    }
    else if(c == "cozlampd"){
        mensagem[0] = '2';        //id do arduino da cozinha
        mensagem[1] =  mensagem [2] = mensagem[3] = mensagem[4] = 'M';
        mensagem[5] = 'D';
        digitalWrite(2, 0);
    	envio(mensagem);
    }
    else if(c== "cozcooll"){
        mensagem[0] = '2';        //id do arduino da cozinha
        mensagem[1] =  'L';
        mensagem [2] = mensagem[3] = mensagem[4] =  mensagem[5] = 'M';
        envio(mensagem);
        lcd_msg = "1Cooler cozinha ligado";
    }
    else if(c== "cozcoold"){
        mensagem[0] = '2';        //id do arduino da cozinha
        mensagem[1] =  'D';
        mensagem [2] = mensagem[3] = mensagem[4] =  mensagem[5] = 'M';
        envio(mensagem);  
    }
    else if(c== "cozaquecl"){
        mensagem[0] = '2';        //id do arduino da cozinha
        mensagem[2] =  'L';
        mensagem [1] = mensagem[3] = mensagem[4] =  mensagem[5] = 'M';
        envio(mensagem);
    }
    else if(c== "cozaquecd"){
        mensagem[0] = '2';        //id do arduino da cozinha
        mensagem[2] =  'D';
        mensagem [1] = mensagem[3] = mensagem[4] =  mensagem[5] = 'M';
        envio(mensagem);  
    }
    //------------------------QUARTO---------------------------------
    else if(c== "quarlaml"){
        mensagem[0] = '3';        //id do arduino da quarto
        mensagem[5] =  'D';
        mensagem [2] = mensagem[3] = mensagem[4] =  mensagem[1] = 'M';
        envio(mensagem);  
    }
    else if(c== "quarlamd"){
        mensagem[0] = '3';        //id do arduino da quarto
        mensagem[5] =  'D';
        mensagem [2] = mensagem[3] = mensagem[4] =  mensagem[1] = 'M';
        envio(mensagem);  
    }
    else if(c== "quarcooll"){
        mensagem[0] = '3';        //id do arduino da quarto
        mensagem[1] =  'D';
        mensagem [2] = mensagem[3] = mensagem[4] =  mensagem[5] = 'M';
        envio(mensagem);  
    }
    else if(c== "quarcoold"){
        mensagem[0] = '3';        //id do arduino da cozinha
        mensagem[1] =  'D';
        mensagem [2] = mensagem[3] = mensagem[4] =  mensagem[5] = 'M';
        envio(mensagem);  
    }
  
    //----------------------GARAGEM---------------------------------
    //Serial.println(c); 
    else if(c== "garlartl"){
        mensagem[0] = '4';        //id do arduino da garagem
        mensagem[1] =  'D';
        mensagem [2] = mensagem[3] = mensagem[4] =  mensagem[5] = 'M';
        envio(mensagem);  
    }
    else if(c== "garlartd"){
        mensagem[0] = '4';        //id do arduino da garagem
        mensagem[1] =  'D';
        mensagem [2] = mensagem[3] = mensagem[4] =  mensagem[5] = 'M';
        envio(mensagem);  
    }
    else if(c== "garportl"){
        mensagem[0] = '4';        //id do arduino da garagem
        mensagem[1] =  'D';
        mensagem [2] = mensagem[3] = mensagem[4] =  mensagem[5] = 'M';
        envio(mensagem);  
    }
    else if(c== "garportd"){
        mensagem[0] = '4';        //id do arduino da garagem
        mensagem[1] =  'D';
        mensagem [2] = mensagem[3] = mensagem[4] =  mensagem[5] = 'M';
        envio(mensagem);  
    }
  }
  else if (vw_get_message(buf, &buflen)){
    for (int i = 0; i < buflen; i++){
           info[i]=buf[i];
      } 
      Serial.println(info{{);
   
  } 
}

void envio(char* msg){

  vw_send((uint8_t *)msg, strlen(msg));
  vw_wait_tx();
}

